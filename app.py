import os
import time
from flask import Flask
from flask import render_template
from flask import redirect, url_for
from flask import request, json

app = Flask(__name__)

@app.route("/")
def index():
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    json_url =os.path.join(SITE_ROOT, "static", "codes.json")
    codes = json.load(open(json_url))
    yearRange=range(0,12)
    return render_template('index.html', codes=codes, yearRange=yearRange)

if __name__ == "__main__":
    #app.run(host="192.168.0.13")
    app.run(host="localhost")




