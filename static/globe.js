$(function(){
  var width = 600,
      height = 600,
      sens = 0.25,
      focused;

  //Setting projection

  var projection = d3.geo.orthographic()
      .scale(245)
      .rotate([0, 0])
      .translate([width / 2, height / 2])
      .clipAngle(90);

  var path = d3.geo.path()
      .projection(projection);

  //SVG container

  var svg = d3.select("body").append("svg")
      .attr("width", width)
      .attr("height", height);

  //Adding water

  svg.append("path")
    .style("fill", "url(#wave)")
    .datum({type: "Sphere"})
    .attr("class", "water")
    .attr("d", path)
    .call(d3.behavior.drag()
          .origin(function() { var r = projection.rotate(); return {x: r[0] / sens, y: -r[1] / sens}; })
          .on("drag", function() {
            var rotate = projection.rotate();
            projection.rotate([d3.event.x * sens, -d3.event.y * sens, rotate[2]]);
            svg.selectAll("path.land").attr("d", path);
            svg.selectAll(".focused").classed("focused", focused = false);
          }));

  var countryTooltip = d3.select("body").append("div").attr("class", "countryTooltip"),
      countryList = d3.select("body").append("select").attr("name", "countries");


  queue()
    .defer(d3.json, "static/world.json")
    .defer(d3.tsv, "static/names-updated.tsv")
    .await(ready);

  //Main function

  function ready(error, world, countryData) {

    var countryNameById = {},
        countryCodeById = {},
        countries = topojson.feature(world, world.objects.countries).features;

    //Adding countries to select

    countryData.forEach(function(d) {
      countryNameById[d.id] = d.name;
      countryCodeById[d.id] = d.code;
      option = countryList.append("option");
      option.text(d.name);
      option.property("value", d.id);
    });

    //Drawing countries on the globe

    var world = svg.selectAll("path.land")
        .data(countries)
        .enter().append("path")
        .attr("class", "land")
        .attr("d", path)
        .attr('data-id', function(d) {
          return d.id;
        })
        .attr('code', function(d) {
          return countryCodeById[d.id];
        })
        .on("click", rotateMe)
    //Drag event

        .call(d3.behavior.drag()
              .origin(function() { var r = projection.rotate(); return {x: r[0] / sens, y: -r[1] / sens}; })
              .on("drag", function() {
                var rotate = projection.rotate();
                projection.rotate([d3.event.x * sens, -d3.event.y * sens, rotate[2]]);
                svg.selectAll("path.land").attr("d", path);
                svg.selectAll(".focused").classed("focused", focused = false);
              }))

    //Mouse events
        .on("click", function(d){rotateMe(d);})
        .on("mouseover", function(d) {
          countryTooltip.text(countryNameById[d.id])
            .style("left", (d3.event.pageX + 7) + "px")
            .style("top", (d3.event.pageY - 15) + "px")
            .style("display", "block")
            .style("opacity", 1);
        })
        .on("mouseout", function(d) {
          countryTooltip.style("opacity", 0)
            .style("display", "none");
        })
        .on("mousemove", function(d) {
          countryTooltip.style("left", (d3.event.pageX + 7) + "px")
            .style("top", (d3.event.pageY - 15) + "px");
        });

    //Country focus on option select

    var rotateMe =  function(d) {
      var rotate = projection.rotate(),
          focusedCountry = country(countries, d.id),
          p = d3.geo.centroid(focusedCountry);
      console.log(getPlayList(countryCodeById[focusedCountry.id]))
      svg.selectAll(".focused").classed("focused", focused = false);

      //Globe rotating

      (function transition() {
        d3.transition()
          .duration(2500)
          .tween("rotate", function() {
            var r = d3.interpolate(projection.rotate(), [-p[0], -p[1]]);
            return function(t) {
              projection.rotate(r(t));
              svg.selectAll("path").attr("d", path)
                .classed("focused", function(d, i) { return d.id == focusedCountry.id ? focused = d : false; });
            };
          })
      })();
    };
    function country(cnt, id) {
      for(var i = 0, l = cnt.length; i < l; i++) {
        if(cnt[i].id == id) {return cnt[i];}
      }
    };

  };
  

  $('#6').click();
});

function getPlayList(country){
  var year = $("#nav li.active input").attr('id');
  var moods = getCheckedBoxes("musicType").split(',');
  var xhr = new XMLHttpRequest();
  xhr.open("put", "http://localhost/api/playlist/121212/next", false);
  xhr.data
  xhr.setRequestHeader("Content-type", "application/json");
  dataToSend=JSON.stringify({decade: year, country: country, moods: moods})
  console.log(dataToSend)
  xhr.send(dataToSend)
  console.log(xhr.response)
  result = JSON.parse(xhr.response)
  console.log(result.song);
  var audio = document.getElementById('oggSource');
  audio.src = result.song.ogg||result.song.mp3
  audio.onended = function() {
    getPlayList(country)
};
  audio.load();
  audio.play();
}
 // Pass the checkbox name to the function
  function getCheckedBoxes(chkboxName) {
    var checkboxes = document.getElementsByName(chkboxName);
    var checkboxesChecked = [];
    var Checked ='';
    // loop over them all
    for (var i=0; i<checkboxes.length; i++) {
      // And stick the checked ones onto an array...
      if (checkboxes[i].checked) {
        checkboxesChecked.push(checkboxes[i]);
      }
    }
    // Return the array if it is non-empty, or null
    if (checkboxesChecked.length > 0) {
      for (var i=0; i<checkboxesChecked.length; i++) {
        Checked = Checked+checkboxesChecked[i].value
        i<(checkboxesChecked.length-1) ? Checked = Checked+',': 'WEIRD';
      }
      return Checked.length > 0 ? Checked : 'WEIRD';
    }
  };
