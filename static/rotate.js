$(function(){
  var nbOptions = 12; // number of menus
  var angleStart = 0; // start angle
  var s = "#nav"
  var t = "#image"
  var li = $(s).find('li');
  var li2 = $(t).find('li');
  var deg = 360/li.length;

  for(var j=0; j<li2.length; j++) {
    var d = j*deg;
    rotate(li2[j],d);
  }
  for(var i=0; i<li.length; i++) {
    var d = i*deg;
    rotate(li[i],d);
  }
  // jquery rotate animation
  function rotate(el, d, prevAngle) {
    var prevAngle = (typeof prevAngle === 'undefined') ? angleStart : prevAngle;
    $({d:prevAngle}).animate({d:d}, {
      step: function(now) {
        $(el)
          .css({ transform: 'rotate('+now+'deg)' })
          .find('label')
          .css({ transform: 'rotate('+(-now)+'deg)' });
      }, duration: 300
    });
  }
  $("#nav li").click(function() {
    var prevAngle = $("#nav li.active").attr('id');
    var num = $("#nav li.active").attr('id');             clear();
    $(this).addClass("active");
    var grad = $('.grad');
    rotate(grad, num*deg, prevAngle*deg);
    updateCountry();
  });
  $(".btn-red").click(function() {
    updateCountry();
  });

  function updateCountry(){
    var year = $("#nav li.active input").attr('id');
    var mood = getCheckedBoxes("musicType");
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost/api/playlist/countriesByTempos/"+year+"?moods="+mood, false);
    xhr.send(null);
    var result = JSON.parse(xhr.response)[year];
    var selectCountry = document.getElementById('select-country');
    d3.selectAll('path').attr('class', 'land');
    result.forEach(function(country){
      d3.select('[code="'+country+'"]').attr('class', 'land availlable');
    });

  };


  function clear(o) {
    $("#nav li").each(function() {
      $(this).removeClass("active");
    });
  }
  // Pass the checkbox name to the function
  function getCheckedBoxes(chkboxName) {
    var checkboxes = document.getElementsByName(chkboxName);
    var checkboxesChecked = [];
    var Checked ='';
    // loop over them all
    for (var i=0; i<checkboxes.length; i++) {
      // And stick the checked ones onto an array...
      if (checkboxes[i].checked) {
        checkboxesChecked.push(checkboxes[i]);
      }
    }
    // Return the array if it is non-empty, or null
    if (checkboxesChecked.length > 0) {
      for (var i=0; i<checkboxesChecked.length; i++) {
        Checked = Checked+checkboxesChecked[i].value
        i<(checkboxesChecked.length-1) ? Checked = Checked+',': 'WEIRD';
      }
      return Checked.length > 0 ? Checked : 'WEIRD';
    }
  };
});
